import numpy as np
import pandas as pd
from scipy.interpolate import PchipInterpolator
from scipy.interpolate import interp1d

# Creating the DataFrame with non-equally spaced rising values
data = {
    'x': [599.997252, 600, 799.965056, 800, 849.962692],
    'y': [18.1, np.nan, 30.7, np.nan, 134]
}
data_manual = {
    'x': [599.997252, 799.965056, 849.962692],
    'y': [18.1, 30.7, 134]
}
# Part 1
df = pd.DataFrame(data_manual)
V600=600
df_closest600 = df.iloc[(df['x']-V600).abs().argsort()[:2]]
V800=800
df_closest800 = df.iloc[(df['x']-V800).abs().argsort()[:2]]

#view results
V1= df_closest600['x'].iloc[0]
V2= df_closest600['x'].iloc[1]
value_at_600 = df_closest600.loc[df_closest600['x'] == V1, 'y'].values[0]
I1 = df_closest600.loc[df_closest600['x'] == V1, 'y'].values[0]
I2 = df_closest600.loc[df_closest600['x'] == V2, 'y'].values[0]
d600=I1+(V600-V1)/(V2-V1)*(I2-I1)
V1= df_closest800['x'].iloc[0]
V2= df_closest800['x'].iloc[1]
value_at_600 = df_closest800.loc[df_closest800['x'] == V1, 'y'].values[0]
I1 = df_closest800.loc[df_closest800['x'] == V1, 'y'].values[0]
I2 = df_closest800.loc[df_closest800['x'] == V2, 'y'].values[0]
d800=I1+(V800-V1)/(V2-V1)*(I2-I1)
division_result = d800/d600
print(f"The value at x=800 divided by the value at x=600 for the manual fit is {division_result}")

# Part 2
df = pd.DataFrame(data)
# Drop rows where 'y' is NaN for fitting the interpolation function
df_known = df.dropna()

# Fit the PCHIP interpolator
interp_func = PchipInterpolator(df_known['x'], df_known['y'])

# Apply the interpolation function to the entire 'x' column
df['y'] = interp_func(df['x'])

# Extracting the specific values
value_at_800 = df.loc[df['x'] == 800, 'y'].values[0]
value_at_600 = df.loc[df['x'] == 600, 'y'].values[0]

# Performing the division
division_result = value_at_800 / value_at_600

# Printing the result
print(f"The value at x=800 divided by the value at x=600 for PchipInterpolator fit is {division_result}")

# Part 3

# Drop rows where 'y' is NaN for fitting the interpolation function
df_known = df.dropna()

# Fit the interpolation function
interp_func = interp1d(df_known['x'], df_known['y'], kind='linear', fill_value="extrapolate")

# Apply the interpolation function to the entire 'x' column
df['y'] = interp_func(df['x'])


# Extracting the specific values
value_at_800 = df.loc[df['x'] == 800, 'y'].values[0]
value_at_600 = df.loc[df['x'] == 600, 'y'].values[0]

# Performing the division
division_result = value_at_800 / value_at_600

# Printing the result
print(f"The value at x=800 divided by the value at x=600 for interp1d fit is {division_result}")
